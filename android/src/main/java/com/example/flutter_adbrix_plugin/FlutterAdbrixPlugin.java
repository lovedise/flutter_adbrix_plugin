package com.example.flutter_adbrix_plugin;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;

import com.igaworks.v2.core.AdBrixRm;
import com.igaworks.v2.core.application.AbxActivityHelper;
import com.igaworks.v2.core.application.AbxActivityLifecycleCallbacks;

import java.util.HashMap;
import java.util.Map;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/** FlutterAdbrixPlugin */
public class FlutterAdbrixPlugin implements MethodCallHandler {
//  Context context;
//  MethodChannel methodChannel;

  Registrar registrar;
  private Application.ActivityLifecycleCallbacks activityLifecycleCallbacks;

  public FlutterAdbrixPlugin(Registrar registrar) {
    this.registrar = registrar;
//    this.context = context;
//    this.methodChannel = methodChannel;
//    this.methodChannel.setMethodCallHandler(this);

    if (Build.VERSION.SDK_INT >= 14) {
      activityLifecycleCallbacks = new AbxActivityLifecycleCallbacks();
    }
  }

  // Plugin이 Flutter Activity와 연결되었을 때 불리는 callback method이다.
//  @Override
//  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
//    final MethodChannel channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "flutter_adbrix_plugin");
//    channel.setMethodCallHandler(new FlutterAdbrixPlugin(flutterPluginBinding.getFlutterEngine().getApplicationContext(), channel));
//  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  public static void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "flutter_adbrix_plugin");
    channel.setMethodCallHandler(new FlutterAdbrixPlugin(registrar));
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);
    } else if(call.method.equals("initSdk")){
      init(call, result);
    } else {
      result.notImplemented();
    }
  }

  private void init(MethodCall call, MethodChannel.Result result) {
    String appKey = (String) call.argument("ADBRIX_APP_KEY");
    String secretKey = (String) call.argument("ADBRIX_SECRET_KEY");
    AbxActivityHelper.initializeSdk(registrar.activity(), appKey, secretKey);

    final Map<String, String> response = new HashMap<>();
    response.put("status", "OK");

    result.success(response);
  }

//  @Override
//  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
//  }
}
