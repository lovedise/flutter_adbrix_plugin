package com.example.flutter_adbrix_plugin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.igaworks.v2.core.AbxReceiver;

public class BrixMultipleInstallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //AdbrixRemastered 구글 인스톨 리시버 등록
        AbxReceiver abxReceiver = new AbxReceiver();
        abxReceiver.onReceive(context,intent);

        //INSTALL_REFERRER 를 전달 받아야 하는 다른 리시버를 등록합니다.
    }
}
