
import 'package:adbrix_sdk/src/adbrix_constants.dart';
import 'package:flutter/services.dart';

class AdbrixSdk {
  final MethodChannel _methodChannel;
  static AdbrixSdk _instance;

  factory AdbrixSdk() {
    if(_instance == null) {
      MethodChannel methodChannel = const MethodChannel(AdbrixConstants.METHOD_CHANNEL);
      _instance = AdbrixSdk.private(methodChannel);
    }

    return _instance;
  }

  AdbrixSdk.private(this._methodChannel);

  Future<dynamic> initSdk(String appKey, String secretKey) async {
    Map<String, dynamic> options = {};

    options[AdbrixConstants.APP_KEY] = appKey;
    options[AdbrixConstants.SECRET_KEY] = secretKey;
    return _methodChannel.invokeMethod('initSdk',options);
  }
}