class AdbrixConstants {
  static const METHOD_CHANNEL = 'flutter_adbrix_plugin';
  static const APP_KEY = 'APP_KEY';
  static const SECRET_KEY = 'SECRET_KEY';
}