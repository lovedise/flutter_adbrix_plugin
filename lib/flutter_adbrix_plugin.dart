library adbrix_sdk;

import 'dart:io';
import 'package:flutter/services.dart';
import 'package:meta/meta.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:core';
import 'package:flutter/material.dart';

export 'src/adbrix_constants.dart';
export 'src/adbrix_sdk.dart';

// import 'dart:async';

// import 'package:flutter/services.dart';
// import 'package:flutter_adbrix_plugin/adbrix_constants.dart';

// class FlutterAdbrixPlugin {
//   static const MethodChannel _channel =
//       const MethodChannel(AdbrixConstants.METHOD_CHANNEL);

//   static Future<String> get platformVersion async {
//     final String version = await _channel.invokeMethod('getPlatformVersion');
//     return version;
//   }

//   static Future<dynamic> initSdk(String appKey, String secretKey) async {
//     return await _channel.invokeMethod('initSdk');
//   }
// }
